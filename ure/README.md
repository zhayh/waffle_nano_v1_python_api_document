# `ure` – 正则表达式

  - [概要](#概要)
  - [`ure` API详解](#ure-api详解)
    - [函数](#函数)
    - [正则表达式对象](#正则表达式对象)
    - [Match 对象](#match-对象)

## 概要

&emsp;&emsp;该模块实现相应CPython模块的子集

&emsp;&emsp;该模块实现正则表达式操作。支持的正则表达式语法是CPython `re` 模块的一个子集（实际上是POSIX扩展正则表达式的子集）。

支持运营商为：

- `'.'`

  匹配任何字符。

- `'[]'`

  匹配字符集。支持单个字符和范围。

- `'^'`

  匹配字符串的开头.

- `'$'`

  匹配字符串的结尾.

- `'?'`

  匹配零个或前一个实体之一.

- `'*'`

  匹配零个或多个先前的实体y.

- `'+'`

  匹配一个或多个先前的实体.

- '??'

  非贪婪模式，匹配零个实体

- '*?'

  非贪婪模式，匹配零个实体
  、
- '+?'

  非贪婪模式，匹配一个实体

- `'|'`

  匹配此运营商的左侧或右侧.

- `'(...)'`

  分组。每个组都在捕获（可以使用match.group()method访问捕获的子字符串）。

&emsp;&emsp;不支持计数重复({m,n})、更高级断言、名称组等。

## `ure` API详解

&emsp;&emsp;使用`import ure`导入`ure`模块

&emsp;&emsp;再使用`TAB` 按键来查看`ure`中所包含的内容：

```python
>>> import ure
>>> ure.
__name__        compile         match           search
sub
```

### 函数

- `ure.compile`(*regex_str*)

  函数说明：编译正则表达式，返回 正则表达式对象对象。

- `ure.match`(*regex_str*, *string*)

  将 正则表达式对象 与 `string` 匹配。匹配通常从字符串的起始位置进行。

- `ure.search`(*regex_str*, *string*)

  函数说明：在 `string` 中搜索 正则表达式对象不同，这将首先搜索与正则表达式相匹配的字符串（若正则表达式固定，则字符串为0）。

- `ure.sub`(*regex_str*, *replace*, *string*, *count=0*, *flags=0*)

  函数说明：编译regex_str并在string中搜索它，将所有匹配项替换为replace，然后返回新字符串。

  replace可以是字符串或函数。如果它是一个字符串然后逃逸形式的序列\<number>和\g<number>可用于扩大到相应的组（或提供无与伦比的基团的空字符串）。如果replace是一个函数，则它必须采用单个参数（匹配项），并应返回替换字符串。

  如果指定了count且非零，那么在进行了如此多的替换后，替换将停止。该标志参数将被忽略。

  注意：此功能的可用性取决于。MicroPython port

- `ure.DEBUG`

  函数说明：标记值，显示有关已编译表达式的调试信息。

### 正则表达式对象

&emsp;&emsp;编译正则表达式。该类实例使用 `ure.compile()`] 创建。

- `regex.match`(*string*)

- `regex.search`(*string*)

- `regex.sub`(*replace*, *string*, *count=0*, *flags=0*)

  与模块级函数 `match()`和 `search()`, `sub`相似。若将同一正则表达式应用于多个字符串，则使用该方法会大大提高效率。

- `regex.split`(*string*, *max_split=-1*)

  使用正则表达式拆分字符串。若给定，则指定将拆分的最大数量。返回字符串列表（若指定，则可能会有多达 *max_split+1* 个元素）。

### Match 对象

&emsp;&emsp;匹配由 `match()` 和 `search()` 方法返回的对象。

- `match.group`([*index*])

  返回匹配（子）字符串。若完全匹配 *index* 为0， 对于每个捕获组为1或更多。 仅支持数字组。

- `match.groups`()

  返回一个包含该匹配组的所有子字符串的元组。

  注意：此方法的可用性取决于。MicroPython port

- `match.start`(**[***index***]**)

- `match.end`(**[***index***]**)

  在匹配的子字符串组的开头或结尾的原始字符串中返回索引。 索引默认为整个组，否则将选择一个组。

  注意：这些方法的可用性取决于。MicroPython port

- `match.span`(**[***index***]**)

  返回2元组。(match.start(index), match.end(index))

  注意：此方法的可用性取决于MicroPython port.