# NFC

&emsp;&emsp;这个模块用于实现动态 NFC  标签读写相关功能。

## NFC初始化

### 函数

&emsp;&emsp;nfc.init()

### 函数说明

&emsp;&emsp;初始化 `NFC` 组件，返回 `True` 则为初始化成功。初始化成功后NFC标签会自动写入初始文本`Hello,I`m Waffle Nano!`的字样。

### 函数示例

```python
import nfc ##导入NFC库
nfc.init() ##完成NFC初始化
```


## 写入URI链接

### 函数

&emsp;&emsp;write_uri(uri_id, uri_string)

### 函数说明

&emsp;&emsp;向动态`NFC`模块写入 `URI` 链接信息，并覆盖之前所有历史标签信息。

### 参数说明

#### uri_id

&emsp;&emsp;uri_id为uri类型，主要包含：

```
URI_ID_0x01          "http://www.\0"
URI_ID_0x02          "https://www.\0"
URI_ID_0x03          "http://\0"
URI_ID_0x04          "https://\0"
URI_ID_0x05          "tel:\0"
URI_ID_0x06          "mailto:\0"
URI_ID_0x07          "ftp://anonymous:anonymous@\0"
URI_ID_0x08          "ftp://ftp.\0"
URI_ID_0x09          "ftps://\0"
URI_ID_0x0A          "sftp://\0"
URI_ID_0x0B          "smb://\0"
URI_ID_0x0C          "nfs://\0"
URI_ID_0x0D          "ftp://\0"
URI_ID_0x0E          "dav://\0"
URI_ID_0x0F          "news:\0"
URI_ID_0x10          "telnet://\0"
URI_ID_0x11          "imap:\0"
URI_ID_0x12          "rtsp://\0"
URI_ID_0x13          "urn:\0"
URI_ID_0x14          "pop:\0"
URI_ID_0x15          "sip:\0"
URI_ID_0x16          "sips:\0"
URI_ID_0x17          "tftp:\0"
URI_ID_0x18          "btspp://\0"
URI_ID_0x19          "btl2cap://\0"
URI_ID_0x1A          "btgoep://\0"
URI_ID_0x1B          "tcpobex://\0"
URI_ID_0x1C          "irdaobex://\0"
URI_ID_0x1D          "file://\0"
URI_ID_0x1E          "urn:epc:id:\0"
URI_ID_0x1F          "urn:epc:tag\0"
URI_ID_0x20          "urn:epc:pat:\0"
URI_ID_0x21          "urn:epc:raw:\0"
URI_ID_0x22          "urn:epc:\0"
URI_ID_0x23          "urn:nfc:\0"
```

#### uri_string

&emsp;&emsp;uri 链接字符串

### 函数示例

```python
import nfc ##导入NFC库
nfc.init() ##完成NFC初始化
nfc.write_uri(nfc.URI_ID_0x02 , "gitee.com/blackwalnutlabs/waffle_nano_v1_python_api_document")  ##链接到API手册地址
```

## 写入TEXT文本标签

### 函数

&emsp;&emsp;write_text(string)

### 函数说明

&emsp;&emsp;向动态`NFC`模块写入文本标签信息，并覆盖之前所有历史标签信息，且文本信息长度不宜超过500字节。

### 参数说明

&emsp;&emsp;标签文本字符串。

### 函数示例

```python
import nfc ##导入NFC库
nfc.init() ##完成NFC初始化
nfc.write_text("Hello,I`m Waffle Nano!") ##写入TEXT标签
```


## 写入包名标签

&emsp;&emsp;write_aar(package_name)

### 函数说明

&emsp;&emsp;向动态`NFC`模块写入程序包名标签信息用于打开包名所指的手机应用，并覆盖之前所有历史标签信息。

### 参数说明

&emsp;&emsp;包名文本字符串。

### 函数示例

```python
import nfc ##导入NFC库
nfc.init() ##完成NFC初始化
nfc.write_aar("hello") ##写入包名
```


## 写入短信标签

&emsp;&emsp;write_sms(phone_number, message_string)

### 函数说明

&emsp;&emsp;向动态`NFC`模块写入短信号码和短信内容标签信息，并覆盖之前所有历史标签信息。

### 参数说明

&emsp;&emsp;`phone_number`:短信手机号字符，`message_string`:短信内容字符串信息。

### 函数示例

```python
import nfc ##导入NFC库
nfc.init() ##完成NFC初始化
write_sms("12345678", "hello") ##向12345678发送`hello`字符串短信。
```

## 写入WIFI信息标签

&emsp;&emsp;write_wifi(ssid, encryption_type, auth_type, password)

### 函数说明

&emsp;&emsp;向动态`NFC`模块写入 `WiFi` 的账号、安全类型、加密方式和密码等WIFI标签信息，并覆盖之前所有历史标签信息。

### 参数说明

&emsp;&emsp;加密方式配置

|encryption_type|
|---|
|WIFI_ENCRYPTION_AES|
|WIFI_ENCRYPTION_NONE|
|WIFI_ENCRYPTION_TKIP|
|WIFI_ENCRYPTION_WEP|

&emsp;&emsp;安全类型配置

|encryption_type|
|---|
|WIFI_AUTH_OPEN|
|WIFI_AUTH_SHARED|
|WIFI_AUTH_WPA|
|WIFI_AUTH_WPA2|
|WIFI_AUTH_WPA2PSK|
|WIFI_AUTH_WPAPSK|

&emsp;&emsp;`ssid` 为WIFI账号名字符串，`password`为WIFI密码名字符串。

### 函数示例

```python
import nfc ##导入NFC库
nfc.init() ##完成NFC初始化
nfc.write_wifi("waffle", nfc.WIFI_ENCRYPTION_AES, nfc.WIFI_AUTH_WPA2PSK, "12345678") ##连接一个名为waffle的wifi。
```

## 读取文本类型标签

&emsp;&emsp;`read_text()`

### 函数说明

&emsp;&emsp;读取当前NFC标签内的文本信息

### 函数示例

```python
import nfc ##导入NFC库
nfc.init() ##完成NFC初始化
nfc.write_text("Hello,I`m Waffle Nano!") ##写入TEXT标签
print(nfc.read_text()) ## 会打印显示`Hello,I`m Waffle Nano!`
```


## 读取uri类型标签

&emsp;&emsp;`read_uri()`

### 函数说明

&emsp;&emsp;读取当前NFC标签内的uri信息

### 函数示例

```python
import nfc ##导入NFC库
nfc.init() ##完成NFC初始化
nfc.write_uri(nfc.URI_ID_0x02 , "gitee.com/blackwalnutlabs/waffle_nano_v1_python_api_document")  ##链接到API手册地址
print(nfc.read_uri()) ## 会打印显示['https://www.', 'gitee.com/blackwalnutlabs/waffle_nano_v1_python_api_document']
```


## 读取wifi类型标签

&emsp;&emsp;`read_wifi()`

### 函数说明

&emsp;&emsp;读取当前NFC标签内的wifi信息

### 函数示例

```python
import nfc ##导入NFC库
nfc.init() ##完成NFC初始化
nfc.write_wifi("waffle", nfc.WIFI_ENCRYPTION_AES, nfc.WIFI_AUTH_WPA2PSK, "12345678") ##连接一个名为waffle的wifi。
print(nfc.read_wifi()) ##显示['waffle', '12345678']
```


