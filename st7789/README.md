# ST7789 屏幕驱动

## 简介

这是基于ST7789芯片的屏幕驱动库，支持 240x240 和 135x240 两种分辨率。驱动仅支持 `RGB565`。

<p align="center">
  <img src="images/IMG_20210702_125458.jpg" alt="ST7789 display photo"/>
</p>

-------------

## ST7789 实例
----------------

要创建 `ST7789` 必须要一个 `machine.SPI` 或 `machine.SoftSPI` 对象和至少两个引脚（`RESET` 和 `DC` 引脚）

``` python
from machine import SPI, Pin
import st7789
spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))

display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))
display.init()
display.draw_string(50, 230, "Powered by BlackWalnuut Labs.")
```

创建 `ST7789` 实例，除了填入 `machine.SPI`，`width` 和 `height` 外，还可以填入其他选参数：

- `reset` 和 `dc`：用于指定屏幕功能引脚

- `xtart` 和 `ystart` 用于指定屏幕的起始点

- `direction` 用于指定屏幕镜像方向，可以指定以下参数（`st7789.NORMAL`，`st7789.YMIRROR`，`st7789.XMIRROR`，`st7789.XYEXCHANGE`）

## ST7789 其他方法

- `ST7789.fill(color)`

  填充颜色

- `ST7789.pixel(x, y, color)`

  指定像素点填充指定颜色

- `ST7789.line(x0, y0, x1, y1, color)`

  从 (`x0`, `y0`) 到 (`x1`, `y1`) 画一根直线

- `ST7789.hline(x, y, length, color)`

  从指定点开始绘制指定长度的横线

- `ST7789.vline(x, y, length, color)`

    从指定点开始绘制指定长度的竖线

- `ST7789.rect(x, y, width, height, color)`

  从指定点 (`x`, `y`) 绘制指定长、宽和颜色的矩形

- `ST7789.fill_rect(x, y, width, height, color)`

  从指定点 (`x`, `y`) 绘制指定长、宽和颜色的实心矩形

- `ST7789.blit_buffer(buffer, x, y, width, height)`

  直接复制 `bytes()` 或 `bytearray()` 的内容到屏幕内部缓冲区

- `ST7789.draw_string(x, y, str, color=st7789.WHITE, bg=st7789.WHITE, size=1, vertical=False, rotate=st7789.ROTATE_0, spacing=1)`
    
    从指定带点 (x, y) 绘制字符串。
    - color: 指定字符串字体颜色
    - bg: 字符串背景颜色，当color和bg相同时，背景色不生效
    - size：字符串字体大小，必须为正整数
    - vertical：启用竖直排列
    - rotate：旋转角度，仅支持 `ROTATE_0`, `ROTATE_90`, `ROTATE_180`, `ROTATE_270`
    - spacing: 字符间距，默认为 `1` 个像素点

- `ST7789.circle(x, y, r, color)`
    从指定带点 (x, y) 绘制半径为 `r` 的 `color` 颜色的空心圆。

- `ST7789.fill_circle(x, y, r, color)`
    从指定带点 (x, y) 绘制半径为 `r` 的 `color` 颜色的实心圆。

模块其他旋转角度定义：`st7789.ROTATE_0`, `st7789.ROTATE_90`, `st7789.ROTATE_180`, `st7789.ROTATE_270`

模块其他颜色定义：`st7789.BLACK`, `st7789.BLUE`, `st7789.RED`, `st7789.GREEN`, `st7789.CYAN`, `st7789.MAGENTA`, `st7789.YELLOW` 和 `st7789.WHITE`

## 模块其他方法
----------------

- `color565(r, g, b)`

  将`R、G、B`颜色转换为 `2` 个字节长度的 `rgb565` 格式

- `map_bitarray_to_rgb565(bitarray, buffer, width, color=WHITE, bg_color=BLACK)`

    将bitarray转换为适合的 rgb565 颜色缓冲区。bitarray 中的位 1 是带颜色的像素，0 是带 bg_color 的像素。

    这是一个为了提高性能提供的方法，可以使用其打印高分辨率字体。推荐一个您可以使用一个很棒的工具 https://github.com/peterhinch/micropython-font-to-py 从 `.ttf` 生成位图字体。