# `ujson` – JSON编码与解码

  - [概要](#概要)
  - [`ujson` API详解](#ujson-api详解)
    - [函数](#函数)

## 概要

&emsp;&emsp;该模块实现相应 `CPython`模块的子集

&emsp;&emsp;该模块允许Python对象和JSON数据格式之间的转换。

## `ujson` API详解

&emsp;&emsp;使用`import ujson`导入`ujson`模块

&emsp;&emsp;再使用`TAB` 按键来查看`ujson`中所包含的内容：

```python
>>> import ujson
>>> ujson.
__name__        dump            dumps           load
loads
```

### 函数

- `ujson.dumps`(*obj*)

  函数说明：返回表示为JSON字符串的 `obj` 。

  示例：

  ```python
  >>> import ujson
  >>> a = {'name': 'wang', 'age': 29}
  >>> ujson.dumps(a)
  '{"name": "wang", "age": 29}'
  >>> print(type(ujson.dumps(a)))
  <class 'str'>
  ```

- `ujson.loads`(*str*)

  函数说明：解析JSON `str` 并返回一个对象。若该字符串未正确排列，则会引发示值误差。

  示例：
  
  ```python
  >>> import ujson
  >>> a = {'name': 'wang', 'age': 29}
  >>> ujson.dumps(a)
  '{"name": "wang", "age": 29}'
  >>> b=ujson.dumps(a)
  >>> ujson.loads(b)
  {'name': 'wang', 'age': 29}
  >>> print(type(ujson.loads(b)))
  <class 'dict'>
  ```
- `ujson.dump`(*obj*,*fp*)

  函数说明：与文件操作结合起来,把表示为JSON字符串的 `obj`放入文件 。

  示例：

  ```python
  >>> import ujson
  >>> a = {'name': 'wang', 'age': 29}
  >>> fp = open('test.txt', 'w')
  >>> ujson.dump(a, fp)
  >>> fp= open('test.txt', 'r')
  >>> fp.read()
  '{"name": "wang", "age": 29}'
  >>> print(type(fp.read()))
  <class 'str'>
  ```

- `ujson.load`(*fp*)

  函数说明：解析JSON `fp` 并返回一个对象。若该字符串未正确排列，则会引发示值误差。

  示例：
  
  ```python
  >>> import ujson
  >>> a = {'name': 'wang', 'age': 29}
  >>> fp = open('test.txt', 'w')
  >>> ujson.dump(a, fp)
  >>> fp= open('test.txt', 'r')
  >>> ujson.load(fp)
  {'name': 'wang', 'age': 29}
  >>> print(type(ujson.load(fp)))
  <class 'dict'>
  ```
  