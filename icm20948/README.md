# icm20948

## 简介

这是基于 `ICM20948` 传感器的驱动文档。

## ICM20948 实例

要创建 `ICM20948` 实例，必须要一个 `machine.I2C` 对象

``` python
from machine import I2C
import icm20948, utime

imu = icm20948.ICM20948(I2C(0), gyro_scale=icm20948.DPS_2000)
```

创建 `ICM20948` 可以填入其他可选参数：


- `gyro_scale` 配置陀螺仪精度，陀螺仪精度参数仅支持传入以下几种值：
    - `icm20948.DPS_250`
    - `icm20948.DPS_500`
    - `icm20948.DPS_1000`
    - `icm20948.DPS_2000`
- `accel_scale` 配置加速度精度，加速度精度参数仅支持传入以下几种值：
    - `icm20948.GPM_2`
    - `icm20948.GPM_4`
    - `icm20948.GPM_8`
- `dmp` 是否启用 `DMP` 融合数据

实例方法

- ICM20948.dataready()

    判断传感器数据是否就绪

- ICM20948.dataupdate()

    更新传感器数据

- ICM20948.acc_x()

    > PS. 只有关闭 `DMP` 才能使用该方法

    获取X轴加速度数据

- ICM20948.acc_y()

    > PS. 只有关闭 `DMP` 才能使用该方法

    获取Y轴加速度数据

- ICM20948.acc_z()

    > PS. 只有关闭 `DMP` 才能使用该方法

    获取Z轴加速度数据

- ICM20948.gyr_x()

    > PS. 只有关闭 `DMP` 才能使用该方法

    获取X轴角速度数据

- ICM20948.gyr_y()

    > PS. 只有关闭 `DMP` 才能使用该方法

    获取Y轴角速度数据

- ICM20948.gyr_z()

    > PS. 只有关闭 `DMP` 才能使用该方法

    获取Z轴角速度数据

- ICM20948.mag_x()

    > PS. 只有关闭 `DMP` 才能使用该方法

    获取X轴磁力计数据

- ICM20948.mag_y()

    > PS. 只有关闭 `DMP` 才能使用该方法

    获取Y轴磁力计数据

- ICM20948.mag_z()

    > PS. 只有关闭 `DMP` 才能使用该方法

    获取Z轴磁力计数据

&emsp;&emsp;示例：

```python
from machine import I2C #导入IIC库
import icm20948, utime #导入icm20948库和utime库
imu = icm20948.ICM20948(I2C(0), gyro_scale=icm20948.DPS_2000,accel_scale=icm20948.GPM_8) #构造陀螺仪对象，设置角速度、加速度精度
if imu.dataupdate() and imu.dataready(): #如果陀螺仪数据已经被更新，并且已经准备好了以后才能读取数据。
    print(imu.acc_x()) #读取X轴加速度数据
    print(imu.acc_y()) #读取Y轴加速度数据
    print(imu.acc_z()) #读取Z轴加速度数据
    print(imu.gyr_x()) #读取X轴角速度数据
    print(imu.gyr_y()) #读取Y轴角速度数据
    print(imu.gyr_z()) #读取Z轴角速度数据
    print(imu.mag_x()) #读取X轴磁场强度数据
    print(imu.mag_y()) #读取Y轴磁场强度数据
    print(imu.mag_z()) #读取Z轴磁场强度数据
```

- ICM20948.quat6()

    > PS. 只有启用 `DMP` 才能使用该方法

    获取六轴融合原始数据

&emsp;&emsp;示例：

```python
from machine import I2C #导入IIC库
import icm20948, utime,math #导入icm20948库、utime库、math库
imu = icm20948.ICM20948(I2C(0),dmp=True) #构造陀螺仪对象，启用DMP算法
if imu.dataupdate() and imu.dataready(): #如果陀螺仪数据已经被更新，并且已经准备好了以后才能读取数据。
    quat_6= imu.quat6() #获取六轴融合原始数据
    q1 = quat_6[0]/math.pow(2, 30) #计算q0,q1,q2,q3四元素
    q2 = quat_6[1]/math.pow(2, 30) #计算q0,q1,q2,q3四元素
    q3 = quat_6[2]/math.pow(2, 30) #计算q0,q1,q2,q3四元素
    q0 = math.sqrt(abs(1-(q1*q1+q2*q2+q3*q3))) #计算q0,q1,q2,q3四元素
```