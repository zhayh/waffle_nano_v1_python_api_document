# Python3 注释

&emsp;&emsp;确保对模块, 函数, 方法和行内注释使用正确的风格

&emsp;&emsp;`Python` 中的注释有单行注释和多行注释：

&emsp;&emsp;`Python` 中单行注释以 `#` 开头，例如：

```python
# 这是一个注释
print("Hello, World!")
```

**1、单引号（'''）**

```python
'''
这是多行注释，用三个单引号
这是多行注释，用三个单引号 
这是多行注释，用三个单引号
'''
print("Hello, World!")
```

执行以上代码，输出结果为：

<center><img src="assets/image/1.png" width="500"/></center>

<center>(注释)</center>

&emsp;

**2、双引号（"""）**

```python
"""
这是多行注释，用三个双引号
这是多行注释，用三个双引号 
这是多行注释，用三个双引号
"""
print("Hello, World!")
```

执行以上代码，输出结果为：

<center><img src="assets/image/1.png" width="500"/></center>

<center>(注释)</center>

&emsp;
