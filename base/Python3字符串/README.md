# Python3 字符串

- [Python访问字符串中的值](#Python访问字符串中的值)
- [Python字符串更新](#Python字符串更新)
- [Python转义字符](#Python转义字符)
- [Python字符串运算符](#Python字符串运算符)
- [Python字符串格式化](#Python字符串格式化)
- [Python三引号](#Python三引号)
- [Unicode字符串](#Unicode字符串)

&emsp;&emsp;字符串是 `Python` 中最常用的数据类型。我们可以使用引号( `'` 或 `"` )来创建字符串。

&emsp;&emsp;创建字符串很简单，只要为变量分配一个值即可。例如：

```python
var1 = 'Hello World!'
var2 = "Runoob"
```
---

## Python访问字符串中的值

&emsp;&emsp;`Python` 不支持单字符类型，单字符在 `Python` 中也是作为一个字符串使用。

&emsp;&emsp;`Python` 访问子字符串，可以使用方括号 `[]` 来截取字符串，字符串的截取的语法格式如下：

```
变量[头下标:尾下标]
```

&emsp;&emsp;索引值以 `0` 为开始值，`-1` 为从末尾的开始位置。

如下实例：

```python
var1 = 'Hello World!'
var2 = "Waffle Nano"
 
print ("var1[0]: ", var1[0])
print ("var2[1:5]: ", var2[1:5])
```

&emsp;&emsp;执行以上程序会输出如下结果：

<center><img src="assets/image/1.png" width="500"/></center>

---

## Python字符串更新

&emsp;&emsp;你可以截取字符串的一部分并与其他字段拼接，如下实例：

```python
var1 = 'Hello World!'
 
print ("已更新字符串 : ", var1[:6] + 'Waffle!')
```


&emsp;&emsp;执行以上程序会输出如下结果：

<center><img src="assets/image/2.png" width="500"/></center>

---

## Python转义字符

&emsp;&emsp;在需要在字符中使用特殊字符时，`python` 用反斜杠 `\` 转义字符。如下表：

| 转义字符 | 描述 | 实例 |
| --- | --- | --- |
| `\`(在行尾时) | 续行符 | `>>> print("line1 \ `<br>`... line2 \` <br>`... line3")`<br>`line1 line2 line3`<br>`>>>` |
| `\\` | 反斜杠符号 | `>>> print("\\")` <br> `\`|
| `\'` | 单引号 | `>>> print('\'')`<br>`'`|
| `\"` | 双引号 | `>>> print('\"')`<br>`"` |
| `\b` | 退格`(Backspace)`  | `>>> print("Hello \b World!")` <br> `Hello World` |
| `\000` |  空 | `>>> print("\000")` <br> `>>>`|
| `\n` | 换行 | `>>> print("\n")`<br><br>`>>>`|
| `\v` | 纵向制表符 | `>>> print("Hello \v World!")`<br>Hello<br>&emsp;&emsp;&emsp;World!<br>`>>>` |
| `\t` | 横向制表符 | `>>> print("Hello \t World!")` <br> Hello      World! <br>`>>>`|
| `\r` |  回车，将 `\r` 后面的内容移到字符串开头，并逐一替换开头部分的字符，直至将 `\r` 后面的内容完全替换完成。| `>>> print("Hello\rWorld!")` <br> `World!` |
| `\f` | 换页 | `>>> print("Hello \v World!")`<br>Hello<br>&emsp;&emsp;&emsp;World!<br>`>>>` |
| `\yyy` | 八进制数，y 代表 0~7 的字符，例如：\012 代表换行。 | `>>> print("\110\145\154\154\157\40\127\157\162\154\144\41")`<br> `Hello World!` |
| `\xyy` |  十六进制数，以 \x 开头，y 代表的字符，例如：\x0a 代表换行 | `>>> print("\x48\x65\x6c\x6c\x6f\x20\x57\x6f\x72\x6c\x64\x21")` <br> `Hello World!` |
| `\other` | 其它的字符以普通格式输出 | |

---

## Python字符串运算符

&emsp;&emsp;下表实例变量 a 值为字符串 "Hello"，b 变量值为 "Python"：

| 操作符 | 描述 | 实例 |
| --- | --- | --- |
| + | 字符串连接 | a + b 输出结果： HelloPython |
| * | 重复输出字符串 | a*2 输出结果：HelloHello |
| [] | 通过索引获取字符串中字符 | a[1] 输出结果 e | 
| [ : ] | 截取字符串中的一部分，遵循 **左闭右开** 原则，str[0:2] 是不包含第 3 个字符的。| 	a[1:4] 输出结果 `ell` |
| in | 	成员运算符 : 如果字符串中包含给定的字符返回 True | 'H' in a 输出结果 True |
| not in  | 成员运算符 : 如果字符串中不包含给定的字符返回 True |  	'M' not in a 输出结果 True |
| r | 原始字符串 : 原始字符串：所有的字符串都是直接按照字面的意思来使用，没有转义特殊或不能打印的字符。 原始字符串除在字符串的第一个引号前加上字母 `r`（可以大小写）以外，与普通字符串有着几乎完全相同的语法。| `print( r'\n' )` |
| % | 格式字符串 | |

实例如下：

```python
a = "Hello"
b = "Python"
 
print("a + b 输出结果：", a + b)
print("a * 2 输出结果：", a * 2)
print("a[1] 输出结果：", a[1])
print("a[1:4] 输出结果：", a[1:4])
 
if( "H" in a) :
    print("H 在变量 a 中")
else :
    print("H 不在变量 a 中")
 
if( "M" not in a) :
    print("M 不在变量 a 中")
else :
    print("M 在变量 a 中")
 
print (r'\n')
```

&emsp;&emsp;执行以上程序会输出如下结果：

<center><img src="assets/image/3.png" width="500"/></center>

---

## Python字符串格式化

&emsp;&emsp;`Python` 支持格式化字符串的输出 。尽管这样可能会用到非常复杂的表达式，但最基本的用法是将一个值插入到一个有字符串格式符 `%s` 的字符串中。

&emsp;&emsp;在 `Python` 中，字符串格式化使用与 `C` 中 `sprintf` 函数一样的语法。

```python
print ("我叫 %s 今年 %d 岁!" % ('小明', 10))
```

以上实例输出结果：

> 我叫 小明 今年 10 岁!

python字符串格式化符号:

| 符 号 | 描 述|
| --- | --- |
| %c | 格式化字符及其ASCII码 |
| %s | 格式化字符串 | 
| %d | 格式化整数 |
| %u | 格式化无符号整型 |
| %o | 格式化无符号八进制数 | 
| %x | 格式化无符号十六进制数 |
| %X | 格式化无符号十六进制数（大写） |
| %f | 格式化浮点数字，可指定小数点后的精度 |
| %e | 用科学计数法格式化浮点数 |
| %E | 作用同%e，用科学计数法格式化浮点数 |
| %g | %f和%e的简写 |
| %G | %f 和 %E 的简写 |
| %p | 用十六进制数格式化变量的地址 |

格式化操作符辅助指令:

| 符 号 | 功 能|
| --- | --- |
| * | 定义宽度或者小数点精度  |
| - | 用做左对齐  |
| + | 在正数前面显示加号( + )  |
| `<sp>` | 在正数前面显示空格 |
| # | 	在八进制数前面显示零('0')，在十六进制前面显示'0x'或者'0X'(取决于用的是'x'还是'X')|
| 0 | 显示的数字前面填充'0'而不是默认的空格  |
| % | '%%'输出一个单一的'%' |
| (var) | 映射变量(字典参数) |
| m.n. | m 是显示的最小总宽度,n 是小数点后的位数(如果可用的话)|

---

## Python三引号 

&emsp;&emsp;`python` 三引号允许一个字符串跨多行，字符串中可以包含换行符、制表符以及其他特殊字符。实例如下:

```python
para_str = """这是一个多行字符串的实例
多行字符串可以使用制表符
TAB ( \t )。
也可以使用换行符 [ \n ]。
"""
print (para_str)
```

以上实例执行结果为：

<center><img src="assets/image/4.png" width="500"/></center>

&emsp;&emsp;三引号让程序员从引号和特殊字符串的泥潭里面解脱出来，自始至终保持一小块字符串的格式是所谓的WYSIWYG（所见即所得）格式的。

&emsp;&emsp;一个典型的用例是，当你需要一块HTML或者SQL时，这时用字符串组合，特殊字符串转义将会非常的繁琐。


```
errHTML = '''
<HTML><HEAD><TITLE>
Friends CGI Demo</TITLE></HEAD>
<BODY><H3>ERROR</H3>
<B>%s</B><P>
<FORM><INPUT TYPE=button VALUE=Back
ONCLICK="window.history.back()"></FORM>
</BODY></HTML>
'''
cursor.execute('''
CREATE TABLE users (  
login VARCHAR(8), 
uid INTEGER,
prid INTEGER)
''')
```


---

## Unicode字符串

&emsp;&emsp;在 `Python2` 中，普通字符串是以 `8` 位 `ASCII` 码进行存储的，而 `Unicode` 字符串则存储为 `16` 位 `unicode` 字符串，这样能够表示更多的字符集。使用的语法是在字符串前面加上前缀 `u`。

&emsp;&emsp;在 `Python3` 中，所有的字符串都是 `Unicode` 字符串。 

 

