# uos – 基本“文件系统”服务

## 概要

&emsp;&emsp;本节简要介绍`Waffle Nano`上的文件系统`uos`，详细介绍如何使用`python`对`Waffle Nano`内部的`SPIFFS`文件系统进行操作。

## 注意：

&emsp;&emsp;1.文件系统在执行任务过程中异常掉电，存在造成正在写入的文件被损坏的风险。对掉电保护要求高的场景，不建议往文件系统中对文件进行任何操作，或者须根据应用场景做好数据备份。

&emsp;&emsp;2.文件系统底层依赖的FLASH存储器件存在常温下10万次读写寿命的限制，建议用户程序尽可能避免对其长期反复进行操作。

&emsp;&emsp;3.目前文件系统不支持多级目录，所有文件都同一保存在根目录下。

&emsp;&emsp;4.文件系统所能管理的文件名最长不能超过31byte。

&emsp;&emsp;5.此文件系统最大支持同时打开32个文件,且删除文件时，如果已经打开32个文件，则必须调用`python`的`File close()`方法关闭其中任意一个文件，否则将无法删除文件。

&emsp;&emsp;6.文件系统分区大小虽然有`44kb`,但其并非全部能用于存储文件数据，有效利用率比较低，具体存储空间大小按照实际使用为准，与文件本身有关。


## 函数

### uos.remove(path)

&emsp;&emsp;函数说明：删除一个文件。

&emsp;&emsp;函数参数：`path`文件名字符串。

&emsp;&emsp;函数返回：

|值|描述|
|---|---|
|False|删除失败或未找到相应文件|
|True|删除成功|

&emsp;&emsp;示例：从`Waffle Nano`中删除一个名为`a.txt`的文件。

```python
>>>import uos
>>>uos.remove("a.txt")
True
```

### uos.uname()

&emsp;&emsp;函数说明：获取系统信息

&emsp;&emsp;函数返回：系统信息字典

&emsp;&emsp;示例：从`Waffle Nano`中获取当前系统信息。

```python
>>>import uos
>>>uos.uname()
(sysname='BlackWalnutOS', release='OpenHarmony-v1.1.1-LTS', version='v0.8.0-beta on 2021-07-22', machine='Waffle Nano with Hi3861V100')
```

### uos.move(old_path, new_path)

&emsp;&emsp;函数说明：移动文件(文件重命名)。

&emsp;&emsp;函数参数：`old_path`旧文件名字符串。`new_path`新文件名字符串。

&emsp;&emsp;函数返回：

|值|描述|
|---|---|
|False|移动失败或未找到相应文件|
|True|移动成功|

&emsp;&emsp;示例：将`Waffle Nano`中的一个名为`a.txt`的文件重命名为`b.txt`。

```python
>>>import uos
>>>uos.move('a.txt','b.txt')
True
```

### uos.copy(old_path, new_path)

&emsp;&emsp;函数说明：文件复制。

&emsp;&emsp;函数参数：`old_path`待复制的文件名字符串。`new_path`新文件的文件名字符串。

&emsp;&emsp;函数返回：

|值|描述|
|---|---|
|False|复制失败或未找到相应文件|
|True|复制成功|

&emsp;&emsp;示例：将`Waffle Nano`中的一个名为`a.txt`的文件复制成为一个名为`b.txt`的文件，并依然保留原有的`a.txt`文件。

```python
>>>import uos
>>>uos.copy('a.txt','b.txt')
True
```

### uos.stat(path)

&emsp;&emsp;函数说明：获取文件大小。

&emsp;&emsp;函数参数：文件名字符串。

&emsp;&emsp;函数返回：文件字节数

&emsp;&emsp;示例：显示`Waffle Nano`中的一个名为`a.txt`的文件大小，如下所示，其大小为1498字节。

```python
>>>import uos
>>>uos.stat("b.txt")
1498
```

### uos.list()

&emsp;&emsp;函数说明：浏览当前`Waffle Nano`上的所有文件。

&emsp;&emsp;函数返回：文件名字符列表。

&emsp;&emsp;示例：显示`Waffle Nano`中所有文件。下述代码运行结果表示在这块`Waffle Nano`上存在`b.txt`和`a.txt`两个文件。

```python
>>>import uos
>>>uos.list()
['b.txt', 'a.txt']
```